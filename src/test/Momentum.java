package test;
// Copyright 2016 TFLab 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


import com.google.common.base.Splitter;
import com.turintech.quantlib.finance.historicaldata.BarHistory;
import com.turintech.quantlib.finance.marketdata.Bar;
import com.turintech.quantlib.finance.trade.OrderNotification;
import com.turintech.quantlib.strategy.Strategy;

import java.util.LinkedList;
import java.util.Queue;

public class Momentum extends Strategy {

	public int i = 0;
	public double[] priceHistory5 = new double[5];
	public double totallong = 0;
	public double totalshort = 0;
	public double zuihoudeqian = 0;
	
    protected class Macd{
    	private final Queue<Double> window = new LinkedList<Double>();
        private final int period;
        private double sum;
        public Macd(int period) {
            assert period > 0 : "Period must be a positive integer";
            this.period = period;
        }
        public void newNum(double num) {
            sum += num;
            window.add(num);
            if (window.size() > period) {
                sum -= window.remove();
            }
        }
        public double getAvg() {
            if (window.isEmpty()) return 0; // technically the average is undefined
            return sum / window.size();
        }
    }
    
    
    
    
    @Override
    protected void onNewData(BarHistory history, Bar bar) {
		System.out.println(bar.getSymbol() + "\t" + bar.getDateTime());
		// if (bar.getDateTime().toString().equals("2014-09-04T00:00")) {
		// Iterable<String> symbols =
		// Splitter.on(',').trimResults().omitEmptyStrings().split("600331");
		// this.setSymbols(symbols);
		// }

		//double[] testData = {1,2,3,4,4,5,4,3,2,1};
        //int[] windowSizes = {3,5};
        //Vector<double> SMA = new Vector<double>();
		if(i < 5){
			priceHistory5[i%5] = bar.getClose();
		}
		else{
			for (int j = 0; j < 4;j++){
				priceHistory5[j] = priceHistory5[j+1];
			}
			priceHistory5[4] = bar.getClose();
		
		}
        Macd ma3 = new Macd(3);
        Macd ma5 = new Macd(5);
        
        final Queue<Double> MACD = new LinkedList<Double>();
        if(i>=4){
        for (double var : priceHistory5) {
            try{
            	ma3.newNum(var);
	            ma5.newNum(var);
	            
//	            System.out.println("Next number = " + var + ", SMA3 = " + ma3.getAvg());
//	            System.out.println("Next number = " + var + ", SMA5 = " + ma5.getAvg());
	            MACD.add(ma3.getAvg()-ma5.getAvg());
	            //System.out.println("MACD = " + (ma3.getAvg()-ma5.getAvg()));
//	            System.out.println();
	            
	            
	            if (ma3.getAvg() < ma5.getAvg() ){
					enterLong(bar.getSymbol(), (long) (1000));
					totallong = totallong +1000*var;
//					System.out.println("Long");
//					System.out.println("totallong = "+totallong);
				} 
	            else if(ma3.getAvg() > ma5.getAvg()) {
					exitLong(bar.getSymbol(), (long) (1000));
					totalshort = totalshort + 1000*var;
//					System.out.println("Short");
//					System.out.println("totalshort = "+ totalshort);
				}
	            else{
//	            	System.out.println("NO");
	            }
            }
			
		    catch (Exception e){
			      System.out.println(e);
		    }
		
		
        }
        zuihoudeqian = totalshort - totallong;
//        System.out.println("qianzaizhe =  "+zuihoudeqian);
        }
        i++;
	}
    
  
    
    
	/**
	 * handle incoming price data
	 */
	

	@Override
	protected void onOrderNotification(OrderNotification on) {

	}

	/**
	 * initalise the strategy
	 */
	@Override
	public void initialize() {
		this.setInitialEquity(1_000_000);
		this.setTradingStart("2013-01-01");
		this.setTradingStop("2015-05-01");

		try {
			Iterable<String> symbols = Splitter.on(',').trimResults().omitEmptyStrings().split("FLWS");
			this.setSymbols(symbols);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setCleanupPositions(true);
		this.setName("SimpleMomentum");
		
	}
	//public double getreturn(double totallong, double totalshort) {
		//return zuihoudeqian = totalshort - totallong;
	//}
		

}
