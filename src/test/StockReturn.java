package test;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

	public class StockReturn {
		public Date startingDate;
		public Date closingDate;
		public ArrayList<Date> stockTradingDates = new ArrayList<Date>();
		public StockReturn(Date start, Date close){
			startingDate = start;
			//System.out.println(startingDate + "***");
			closingDate = close;
		}
	public void filterDate() {
	//read .csv
	
	String fileName = "/Users/ljt/Desktop/dissertation/backtesting_Java/backtesting/doc/FLWS.csv";
	File file = new File(fileName);//read about file
	//System.out.println(file);
		try{
			Scanner inputStream = new Scanner (file);
			inputStream.next();//ignore the first line
			inputStream.next();

			
			while (inputStream.hasNext()){
				String data =  inputStream.next();//get a whole line
				String[] values=data.split(",");
				
				String dates = values[1];
				dates = dates.replace("\"", "");
				//System.out.println(dates + "***");
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				try{
					date = formatter.parse(dates);
                    //Date tradingdate = date;
				    //System.out.println(tradingdate + "*&*");
				}
				catch(ParseException e){
					e.printStackTrace();
				}
				if(date.compareTo(startingDate) >=0 && closingDate.compareTo(date) >=0){
					
					this.stockTradingDates.add(date);

//				      }
				    
				}

				}

			inputStream.close();
	    } 
		catch(FileNotFoundException e){
		}
	}
	public static void main(String[] args){
		DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String startDate = "2013/01/1";
		String closeDate = "2015/05/1";
		Date start = new Date();
		Date close = new Date();
		try{
			start = formatter.parse(startDate);
			close = formatter.parse(closeDate);		
			}
		catch(ParseException e){
			e.printStackTrace();
		}
		StockReturn stockReturn = new StockReturn(start, close);
		stockReturn.filterDate();
	    
		for (int i = 0; i < stockReturn.stockTradingDates.size();i++){
           			
			System.out.println(stockReturn.stockTradingDates.get(i));
			
		}
		System.out.println(stockReturn.stockTradingDates.size());
		
	}
	
}