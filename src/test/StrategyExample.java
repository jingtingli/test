package test;
// Copyright 2016 TFLab 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


import com.google.common.base.Splitter;
import com.turintech.quantlib.finance.historicaldata.BarHistory;
import com.turintech.quantlib.finance.marketdata.Bar;
import com.turintech.quantlib.finance.trade.OrderNotification;
import com.turintech.quantlib.strategy.Strategy;


public class StrategyExample extends Strategy {

	public int i = 0;

	/**
	 * handle incoming price data
	 */
	@Override
	protected void onNewData(BarHistory history, Bar bar) {
		System.out.println(bar.getSymbol() + "\t" + bar.getDateTime());
		// if (bar.getDateTime().toString().equals("2014-09-04T00:00")) {
		// Iterable<String> symbols =
		// Splitter.on(',').trimResults().omitEmptyStrings().split("600331");
		// this.setSymbols(symbols);
		// }

		if (i % 2 == 0) {
			try {
				enterLong(bar.getSymbol(), (long) (1000));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				exitLong(bar.getSymbol(), (long) (1000));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		i++;
	}

	@Override
	protected void onOrderNotification(OrderNotification on) {

	}

	/**
	 * initalise the strategy
	 */
	@Override
	public void initialize() {
		this.setInitialEquity(1_000_000);
		this.setTradingStart("2013-01-01");
		this.setTradingStop("2015-05-01");

		try {
			Iterable<String> symbols = Splitter.on(',').trimResults().omitEmptyStrings().split("FLWS");
			this.setSymbols(symbols);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setCleanupPositions(true);
		this.setName("SimpleMomentum");
	}

}
