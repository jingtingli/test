package test;
// Copyright 2016 TurinTech 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import com.turintech.quantlib.broker.BackTestingBroker;
import com.turintech.quantlib.broker.BackTestingDataFeed;
import com.turintech.quantlib.broker.LocalDataFeed;
import com.turintech.quantlib.broker.TurinLabDataFeed;
import com.turintech.quantlib.strategy.Strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {
	private static Strategy strategy = new Momentum();

	private static BackTestingBroker broker;
	private static BackTestingDataFeed dataFeed;

	public static void main(String[] args) {
		broker = new BackTestingBroker();
		dataFeed = new LocalDataFeed();

		((TurinLabDataFeed) dataFeed).setDefaultInstrument("STOCK");
	    

		broker.setDataFeed(dataFeed);
		strategy.setBroker(broker);
 
		// Run the strategy
		long start = System.nanoTime();
		strategy.start();
		long elapsedTime = System.nanoTime() - start;
		System.out.println("backtest took " + String.format("%.2f secs", (double) elapsedTime / 1e9));

		start = System.nanoTime();

		strategy.evaluate();
		////////////////////////////////////////////////
		//produced performance data after this step!!!!!
        ////////////////////////////////////////////////
		
		//strategy.getExecutions();
		//strategy.getEndEquities();
		
		StrategyEvaluation perform=new StrategyEvaluation(strategy);
		double sharpRatio,inforRatio,sortinoRatio,beta,maxDrawDown;
		sharpRatio = perform.getSharpeRatio();
		inforRatio=perform.getInforRatio();
		sortinoRatio=perform.getSortinoRatio();
		beta=perform.getBeta();
		maxDrawDown=perform.getMaxDrawDown();
		

		
		
        //Double[] ratioValue=new Double[5];
        //ratioValue = {perform.getSharpeRatio(),perform.getBeta(),perform.getInforRatio(),perform.getSortinoRatio(),perform.getMaxDrawDown()};
		
		elapsedTime = System.nanoTime() - start;
		//System.out.println("writing to the file system took " + String.format("%.2f secs", (double) elapsedTime / 1e9));
		//System.out.println();
		

//		System.out.println("Please enter the number of criteria between 1 and 9: ");
//		Scanner sca0 = new Scanner (System.in);
		int size = 5;
		System.out.println("The size is :" + size);
		Double [][] matrix=new Double [size][size];


		Scanner user_input = new Scanner(System.in);
		System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Information Ratio");
		double sca1 = user_input.nextDouble();
		matrix[0][1] = sca1 ;
		System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Sortino Ratio");
		double sca2 = user_input.nextDouble();
		matrix[0][2] = sca2 ;
		System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Beta");
		double sca3 = user_input.nextDouble();
		matrix[0][3] = sca3 ;
		System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Maximum Drawdown");
		double sca4 = user_input.nextDouble();
		matrix[0][4] = sca4 ;
		System.out.println("Please enter the importance of criteria Information Ratio/Sortino Ratio ");
		double sca5 = user_input.nextDouble();
		matrix[1][2] = sca5 ;
		System.out.println("Please enter the importance of criteria Information Ratio/Beta ");
		double sca6 = user_input.nextDouble();
		matrix[1][3] = sca6 ;
		System.out.println("Please enter the importance of criteria Information Ratio/Maximum Drawdown ");
		double sca7 = user_input.nextDouble();
		matrix[1][4] = sca7 ;
		System.out.println("Please enter the importance of criteria Sortino Ratio/Beta");
		double sca8 = user_input.nextDouble();
		matrix[2][3] = sca8 ;
		System.out.println("Please enter the importance of criteria Sortino Ratio/Maximum Drawdown");
		double sca9 = user_input.nextDouble();
		matrix[2][4] = sca9 ;
		System.out.println("Please enter the importance of criteria Beta/Maximum Drawdown");	
		double sca10 = user_input.nextDouble();
		matrix[3][4] = sca10 ;
		
		 for(int i=0;i<size;i++){
	            matrix[i][i]=1.0;
	        }
	        
	        for(int i=0; i<size; i++){
	        	for(int j=0; j<size; j++){
	        		matrix[j][i]=1/matrix[i][j];
	        		//System.out.printf("%.2f\t", matrix[i][j]);
	        	}
	        	System.out.println();
	        }
	        Double[]matrixColumn= new Double[size];
	        for(int i=0;i<size;i++){
	            	matrixColumn[i]=matrix[i][0]*matrix[i][1]*matrix[i][2]*matrix[i][3];
	                //System.out.println(matrixColumn[i]+"@@@@"); 
	        }
			
	        Double[] line=new Double[size];
	        for(int i=0;i<size;i++){
	                if(matrixColumn[i]!=null){
	                    line[i]=Math.pow(matrixColumn[i],1.0/4);
	                }else{
	                    line[i]=matrixColumn[i];	                
	            }	 
	                //System.out.println(line[i]+"$%%$"); 
	        }
			

	        Double sum=0.0;
	        for(int i=0;i<size;i++){
	            sum=sum+line[i];
	        }
	        //System.out.println(sum+"****"); 
	        
	        Double[] w=new Double[size];
	        for(int i=0;i<size;i++){
	            w[i]=line[i]/sum;    //特征向量
	            //System.out.println(w[i]+"^^^^"); 
	        }
	        
	        Double[] aw=new Double[size];
	        for(int i=0;i<size;i++){
	                    aw[i]=matrix[i][0]*w[0]+matrix[i][1]*w[1]+matrix[i][2]*w[2]+matrix[i][3]*w[3];
	            //System.out.println(aw[i]+"$$$^^^"); 
	        }
	        
	        Double lamda=0.0;                        //最大特征跟R
	        for(int i=0;i<size;i++){
	            lamda=lamda+(aw[i]/(size*w[i]));

	        }
            //System.out.println(lamda+"~~~~"); 
	        
	        Double ci=Math.abs((lamda-size)/(size-1));                //矩阵一致性指标
	        System.out.println("Consistency Index"+ci+"\n");
	        Double [] r={0.0, 0.0, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};
	        Double cr=ci/r[size-1];                        //随机一致性比率 0.9为4阶矩阵的平均一致性指标
	        Double score = (double) 0;
	        if(cr>=0.2){
	            System.out.println("WEIGHTS NOT RELIABLE");
	        }
	        else{
	            //输出特征向量
	            for(int i=0;i<size;i++){
	                System.out.println("Criteria"+i +" weights："+w[i]);
	            }
//	            System.out.println(a + "!!!");
//	            System.out.println(b + "!!!");
//	            System.out.println(c + "!!!");
//	            System.out.println(d + "!!!");
//	            System.out.println(e + "!!!");
	            //score = a * w[0] + c * w[2] + d * w[3] + e * w[4];
	            score = Math.abs(sharpRatio) * w[0] + Math.abs(inforRatio) * w[1] + Math.abs(sortinoRatio) * w[2] + Math.abs(beta) * w[3] + maxDrawDown * w[4];
     	        System.out.println("Overall Score:"+score);
	        }
	  	    }	

}

