package test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import com.google.common.base.Splitter;
import com.google.common.collect.Queues;
import com.turintech.quantlib.finance.historicaldata.BarHistory;
import com.turintech.quantlib.finance.marketdata.Bar;
import com.turintech.quantlib.finance.trade.OrderNotification;
import com.turintech.quantlib.strategy.Strategy;

public class MeanReversionStrategy extends Strategy{
	
	protected double dev_Multiplier=0.5;// multiplier of stdev.
	
	protected int max_notional=1000000;//maximum position
	protected int  min_notional=-1000000;//min position
	
	protected int i=0;

	/**
	 * initialize strategy
	 */
	@Override
	public void initialize() {
		// initialize the strategy with initial paramters
		this.setTradingStart("2004-12-01");
		this.setTradingStop("2014-12-05");
		this.setInitialEquity(1_000_000);
		
		try {
			Iterable<String> symbols = Splitter.on(',').trimResults().omitEmptyStrings().split("JRS");
			this.setSymbols(symbols);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setCleanupPositions(true);
		this.setName("MeanReversion");
	}

	/**
	 * turn on order notification
	 */
	@Override
	protected void onOrderNotification(OrderNotification on) {
		// TODO Auto-generated method stub
	}

	@Override
	/**
	 * handling incoming data
	 */
	protected void onNewData(BarHistory history, Bar bar) {
		
		
		// Strategy execution
		System.out.println(bar.getSymbol() + "\t" + bar.getDateTime());
		
		
		// price history of past 20 days
		double[]priceHistory=new double[20];
		priceHistory[i%20]=bar.getClose();
		
		if (i%20==0 && i>19){
			try{
				
				double moving_average=getLinearRegressionIntercept(priceHistory);
				
				double moving_stdev=getStdev(priceHistory);
				
				double upperBand= moving_average+dev_Multiplier*moving_stdev;
				
				double lowerBand=moving_average-dev_Multiplier*moving_stdev;
				
				
				if (bar.getClose() < lowerBand ){
					enterLong(bar.getSymbol(), (long) (5000));
					System.out.println("Long");
				} else if(bar.getClose()>upperBand) {
					enterShort(bar.getSymbol(), (long) (5000));
					System.out.println("Short");
				}
				
			} catch (Exception e){
				System.out.println(e);
			}
		}
	
	i++;
	}
	/**
	 * 
	 * @param pastPrice
	 * @return
	 */
	private double getLinearRegressionIntercept(double[] pastPrice ){
		
		int timeWindow=pastPrice.length;
				
		double [] days=new double[timeWindow];
		//the regression date parameter
		for(int j=0;j<timeWindow;j++){
			if(j>i%20){
				days[j]=20+i%20+1-j;
			} else {
				days[j]=i%20+1-j;
			}
		}
		
		LinearRegression simpleLinear=new LinearRegression(days, pastPrice);
		
		double intercept=simpleLinear.intercept();
		
		return intercept;
	}		
	private double getStdev(double[] pastPrice){	
		// the sample mean
		double mean=0;
		for(int j=0;j<pastPrice.length;j++){
			mean+=pastPrice[j];
		}
		mean=mean/pastPrice.length;
		
		// the sample stdev
		double stdev=0;
		for (int j=0;j<pastPrice.length;j++){
			stdev+=Math.pow(pastPrice[j]-mean,2);
		}
		stdev=stdev/(pastPrice.length-1);
		
		return Math.sqrt (stdev);
	}
}
