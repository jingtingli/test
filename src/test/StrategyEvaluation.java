package test;
//package backtesting;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.base.Splitter;
import com.turintech.quantlib.finance.historicaldata.Series;
import com.turintech.quantlib.finance.metrics.EndEquity;
import com.turintech.quantlib.finance.metrics.risk.SharpeRatio;
import com.turintech.quantlib.finance.metrics.risk.SortinoRatio;
import com.turintech.quantlib.strategy.Strategy;


public class StrategyEvaluation {
	//name of the strategy
	protected static String name;
	//imported from 
	protected static List<EndEquity> equity;
	protected static List executions;
	protected static Series Pnl;
	protected static Series asdad;
	//protected static List<closingPrice> closingPrice;
	
	// read from library
	private double sharpR;//sharpe ratio
	private double sortinoR;//sortino ratio
	private double mDD;// maximum draw down
	
	//calculate using trade data
	private double beta;// sensitivity to market movement
	private double largeloss;// large loss frequency indicator
	private double inforR;// infromation ratio with bench mark to index
	private double diversify;// diversification level;
	
	private MarketReturn marketReturn;
	private StockReturn stockReturn;
	private Date start;
	private Date close;
	private ArrayList<Double> benchMarkRatios = new ArrayList<Double>();
	private ArrayList<Date> tradingDates = new ArrayList<Date>();
	private ArrayList<Date> stockDates = new ArrayList<Date>();
	private double benchMarkAverage;
	/**
	 * @param Strategy 
	 * initilization
	 */
	public StrategyEvaluation(Strategy s) {
		DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		
        		String startDate = "2013/01/1";
        		String closeDate = "2015/05/1";
		
//		String startDate=s.getEndEquities().get(1).getTimestamp().getYear().to
//		
//		
//		int duration=s.getEndEquities().size();
//		
//		String closeDate=s.getEndEquities().get(duration-1).getTimestamp().
		
//		System.out.println(startDate+"\t"+closeDate);
		
		//get the string from trading start date and end date
		
		
		
		start = new Date();
		close = new Date();
		try{
			start = formatter.parse(startDate);
			close = formatter.parse(closeDate);
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		
		// read data from start to end date
		
		marketReturn = new MarketReturn(start,close);
		stockReturn = new StockReturn(start, close);
		stockReturn.filterDate();
		marketReturn.computeRatio();
		benchMarkRatios = marketReturn.BenchmarkRatio;
		benchMarkAverage = marketReturn.marketReturnAverage;
		/*
		 * 
		 */
		this.tradingDates = marketReturn.tradingDates;
		this.stockDates = stockReturn.stockTradingDates;
		name=s.getName();
		double rf = 0.02;
		sharpR=0;//sharpe ratio
		sortinoR=0;//sortino ratio
		mDD=0;// maximum draw down
		//
		beta=0;// sensitivity to market movement
		inforR=0;// infromation ratio with bench mark to index
		diversify=0;//diversification level
	
		// get the strategy out put
		equity=s.getEndEquities();
		System.out.println("equity size: "+equity.size() +"****");
//	    for (int i=0; i<equity.size();i++){
//           System.out.println(equity.get(i).getEquity());
//        }
		 
		executions=s.getExecutions();
	}
	   //closingPrice = 
			   


	/**
	 * all the getters
	 */
	
	//calculate daliy returns
	private double[] getReturns() {
		double[] arr = new double[equity.size()];
		for (int i = 1; i < equity.size(); i++) {	
			arr[i - 1] = (equity.get(i).getEquity()/ equity.get(i-1).getEquity()) - 1;
		}
		return arr;
	}
	
	protected String getName(){
		return name;
	}
	

	/**
	 * SharpRatio
	 */
	
	protected double getSharpeRatio(){
		double [] returns=getReturns();
		     
		    double mean=getMean(returns);
		    System.out.println(mean +"@@@@~~~");
		    double stdev=getStdev(returns);
		    System.out.println(stdev +"@@@@%%%%%%");
		    sharpR= (mean-0.02)/stdev;		     
		    System.out.println("sharpR: " + sharpR +"????");
		    return sharpR;
		}
	
	
    /**
	 * InformationRatio
	 */

	protected double getInforRatio(){		
	     
	    double [] returns=getReturns();
	    
	    double mean=getMean(returns);
	    //System.out.println(mean +"@@@@~~~");
	    double trackingError=getTrackingError(returns);
	    //System.out.println(trackingError +"@@@@");
	    inforR= (mean-marketReturn.marketReturnAverage)/trackingError;
	     
	    System.out.println("Information Ratio" + inforR);		
        return inforR;
    }
	
	
   /**
    * SortinoRatio
    */
	protected double getSortinoRatio() {

		double[] returns = getReturns();
		//System.out.println(returns.length);
		double SRmean = getMean(returns);
		int counter=0;
		for (int i = 0; i < returns.length; i++) {
		if (returns[i] < 0){
		counter++;
		}
		}
		//System.out.println(counter);

		double[] negreturns = new double[counter]; 
		counter=0;
		for (int i = 0; i < returns.length; i++) {
		if (returns[i] < 0){
		negreturns[counter]=returns[i];
		counter++;}
		}

		sortinoR=(SRmean-0.02)/(getStdev(negreturns));
		System.out.println("Sortino Ratio" + sortinoR);
		return sortinoR;
    }

    /**  
	 * MaximumDrawDown
	 */
	protected double getMaxDrawDown() {
		double[] returns = getReturns();
		double Peak =0;
		        double Trough =0;
		        double MaxDrawDown = 0;
		        double tmpDrawDown = 0;
		    for(int i=0;i<returns.length;i++){
		    if(returns[i]>Peak){
		        Peak =returns[i];
		        Trough=Peak;
		    }
		    else if(returns[i]<Trough){
		    Trough=returns[i];
		    tmpDrawDown = Peak - Trough;
		    }
		    }
		        if(tmpDrawDown>MaxDrawDown){
		        MaxDrawDown=tmpDrawDown;
		        }
		        System.out.println("MaxDrawDown" + MaxDrawDown);
		        return MaxDrawDown;
		}

	protected double getBeta(){
		
		double d = marketReturn.marketReturnAverage;
	     
	    double [] returns=getReturns();
	    double mean=getMean(returns);
	    double Covariance=getCovariance(returns);
	    double marketReturnstd = getStdev(returns);
	     
	    beta= Covariance/marketReturnstd;
	     
	    System.out.println("Beta : " + beta);
	    
		return beta;
	}
	
	
	//protected double getDiversify(){
		//return diversify;
	//}
	/**
	 * get performance ratio of target strategy
	 * @param s
	 * @return double[] ratios
	 */
	
	private double getMean(double[] arr){	
		// the sample mean
		double mean=0;
		for(int j=0;j<arr.length;j++){
			mean+=arr[j];
		}
		return mean=mean/arr.length;
	}
	
	private double getStdev(double[] equity){	
		// the sample mean
		
		double mean=getMean(equity);
		
		//calculate std
		double sum=0;
		double stdev=0;
		for(int i=0; i<equity.length; i++){
			sum += ((double)equity[i] -mean) * (equity[i] -mean);
		}
		System.out.println(sum+"£££££");
		stdev=sum/(equity.length-1);
		
		return Math.sqrt(stdev);
	}
	
	private double getTrackingError(double[] arr){	
		// the sample mean
		double portmean=getMean(arr);
		//calculate std
		double sum=0; 
		double TrackingError=0;
		for(int j=0; j<arr.length; j++)
		    for(int i=0; i<marketReturn.BenchmarkRatio.size(); i++){
			    if(tradingDates.get(i).compareTo(stockDates.get(j)) ==0){
			        sum += ((double)arr[j] - portmean) * (marketReturn.BenchmarkRatio.get(i) - marketReturn.marketReturnAverage);	    
			    }
		}
		TrackingError=Math.abs(sum/(arr.length-1));
		//System.out.println(TrackingError+"£££££");
		return Math.sqrt(TrackingError);
		
	}
	
	private double getCovariance(double[] equity){			
		// the sample mean
		double portmean=getMean(equity);
		//calculate std
		double sum=0; 
		double Cov=0;
		//System.out.println("market Return size: "+marketReturn.BenchmarkRatio.size());
//		System.out.println("return size: "+equity.length);
		marketReturn.BenchmarkRatio.size();
			
		for(int i=0; i<equity.length; i++){
			sum += ((double)equity[i] - portmean) * 
					(marketReturn.BenchmarkRatio.get(i)
							- marketReturn.marketReturnAverage);
		}
		Cov=sum/(equity.length-1);
//		System.out.println(Cov);
		return Cov;
	}

//	public static void main(String[] args){
//		StrategyEvaluation e = new StrategyEvaluation(new Momentum());
//		e.getSharpeRatio();	
//	}
	
}

