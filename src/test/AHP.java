package test;
//package backtesting;
import java.util.Scanner;


public class AHP {

	 public static void main(String[] args) {
		 //input number of criteria used
//		 System.out.println();
//		 @SuppressWarnings("resource")
//		Scanner scanner = new Scanner(System.in);
//		 int n = scanner.nextInt();
//	     double matrix[][] = new double[n][n];
		 
		 
			System.out.println("Please enter the number of criteria between 1 and 9: ");
			Scanner sca0 = new Scanner (System.in);
			int size = sca0.nextInt();
			System.out.println("The size is :" + size);
			Double [][] matrix=new Double [size][size];


			Scanner user_input = new Scanner(System.in);
			System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Information Ratio");
			double sca1 = user_input.nextDouble();
			matrix[0][1] = sca1 ;
			System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Sortino Ratio");
			double sca2 = user_input.nextDouble();
			matrix[0][2] = sca2 ;
			System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Beta");
			double sca3 = user_input.nextDouble();
			matrix[0][3] = sca3 ;
			System.out.println("Please enter the pair comparision of criteria Sharp Ratio/Maximum Drawdown");
			double sca4 = user_input.nextDouble();
			matrix[0][4] = sca4 ;
			System.out.println("Please enter the importance of criteria Information Ratio/Sortino Ratio ");
			double sca5 = user_input.nextDouble();
			matrix[1][2] = sca5 ;
			System.out.println("Please enter the importance of criteria Information Ratio/Beta ");
			double sca6 = user_input.nextDouble();
			matrix[1][3] = sca6 ;
			System.out.println("Please enter the importance of criteria Information Ratio/Maximum Drawdown ");
			double sca7 = user_input.nextDouble();
			matrix[1][4] = sca7 ;
			System.out.println("Please enter the importance of criteria Sortino Ratio/Beta");
			double sca8 = user_input.nextDouble();
			matrix[2][3] = sca8 ;
			System.out.println("Please enter the importance of criteria Sortino Ratio/Maximum Drawdown");
			double sca9 = user_input.nextDouble();
			matrix[2][4] = sca9 ;
			System.out.println("Please enter the importance of criteria Beta/Maximum Drawdown");	
			double sca10 = user_input.nextDouble();
			matrix[3][4] = sca10 ;


//			//double[] sca =  {sca1,sca2,sca3,sca4};
//			for(int i=0;i<size;i++){
//	            matrix[i][i]=1.0;
//	        }
//			for(int i=0; i<size;i++){
//				for(int j=0;j<size;j++){
//					if(j>i){
//						//System.out.println(sca[i]);
//						matrix[i][j]=sca[i]/sca[j];
//					}
//					else if(j==i){
//			            matrix[i][i]=1.0;
//					}
//					else{
//						matrix[i][j]=1/matrix[j][i];
//					}
//					//System.out.printf("%.2f\t", matrix[i][j]);
//				}
//				
//			}
			
	        for(int i=0;i<size;i++){
	            matrix[i][i]=1.0;
	        }
	        
	        for(int i=0; i<size; i++){
	        	for(int j=0; j<size; j++){
	        		matrix[j][i]=1/matrix[i][j];
	        		//System.out.printf("%.2f\t", matrix[i][j]);
	        	}
	        	System.out.println();
	        }
			
//			for(int i=0;i<size;i++){
//	            matrix[i][i]=1.0;
//	        }
//			for(int i=0; i<size;i++){
//				for(int j=0;j<size;j++){
//					if(j==i){
//			            matrix[i][i]=1.0;
//					}
//					else{
//						matrix[i][j]=1/matrix[j][i];
//					}
//					//System.out.printf("%.2f\t", matrix[i][j]);
//				}
//				
//			}


	        //Comparison Matrix Formation depends on perference
//	        matrix[0][1]=2.0;
//	        matrix[0][2]=0.25;
//	        matrix[0][3]=1.0;
//	        matrix[1][2]=0.125;
//	        matrix[1][3]=1.0;
//	        matrix[2][3]=1.0;
	        
	        //input the other elements in the judgment matrix
	        

//	        //sum of the judgment matrix
//
//	        Double[] column=new Double[size];
//	        for(int j=0;j<size;j++){
//	            for(int i=0;i<size;i++){
//	                if(column[j]!=null){
//	                    column[j]=column[j]+matrix[i][j];			//算出每一行的和
//	                }else{
//	                    column[j]=matrix[i][j];
//	                }
//	            }	            
//	        }
//	        
//	        //
//	        Double[][] matrixColumn= new Double[size][size];
//	        for(int j=0;j<size;j++){
//	            for(int i=0;i<size;i++){
//	                matrixColumn[i][j]=matrix[i][j]/column[j];   //归一化
//	                
//	            }	            
//	        }
//	        
//	      //获得行数组
//	        Double[] line=new Double[size];
//	        for(int i=0;i<size;i++){
//	            for(int j=0;j<size;j++){
//	                if(line[i]!=null){
//	                    line[i]=line[i]+matrixColumn[i][j];
//	                }else{
//	                    line[i]=matrixColumn[i][j];
//	                }
//	            }	            
//	        }
	        
	        Double[]matrixColumn= new Double[size];
	        //Double[]matrixColumn = {1.0,1.0,1.0,1.0,1.0};
	        for(int i=0;i<size;i++){
	            //for(int j=1;j<6;j++){
	                //matrixColumn[i]=matrixColumn[j-1]*matrix[i][j-1];   //归一化
	            	matrixColumn[i]=matrix[i][0]*matrix[i][1]*matrix[i][2]*matrix[i][3];
	                //System.out.println(matrixColumn[i]+"@@@@"); 
	            //}
	        }
			
	        Double[] line=new Double[size];
	        for(int i=0;i<size;i++){
	                if(matrixColumn[i]!=null){
	                    line[i]=Math.pow(matrixColumn[i],1.0/4);
	                }else{
	                    line[i]=matrixColumn[i];	                
	            }	 
	                //System.out.println(line[i]+"$%%$"); 
	        }
			

	        Double sum=0.0;
	        for(int i=0;i<size;i++){
	            sum=sum+line[i];
	        }
	        //System.out.println(sum+"****"); 
	        
	        Double[] w=new Double[size];
	        for(int i=0;i<size;i++){
	            w[i]=line[i]/sum;    //特征向量
	            //System.out.println(w[i]+"^^^^"); 
	        }
	        
	        Double[] aw=new Double[size];
	        for(int i=0;i<size;i++){
	            //for(int j=0;j<size;j++){
	               // if(matrix[i][j]!=null){
	                    aw[i]=matrix[i][0]*w[0]+matrix[i][1]*w[1]+matrix[i][2]*w[2]+matrix[i][3]*w[3];
	               // }else{
	                 //   aw[i]=matrix[i][j]*w[j];
	               // }    
	           // }
	            //System.out.println(aw[i]+"$$$^^^"); 
	        }
	        
	        Double lamda=0.0;                        //最大特征跟R
	        for(int i=0;i<size;i++){
	            lamda=lamda+(aw[i]/(size*w[i]));

	        }
            //System.out.println(lamda+"~~~~"); 
	        
//	        Double sumR=0.0;                        //最大特征跟R
//	        for(int i=0;i<size;i++){
//	            sumR=sumR+bw[i]/(size*w[i]);
//	        }
	        Double ci=Math.abs((lamda-size)/(size-1));                //矩阵一致性指标
	        System.out.println("Consistency Index"+ci+"\n");
	        Double [] r={0.0, 0.0, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};
	        Double cr=ci/r[size-1];                        //随机一致性比率 0.9为4阶矩阵的平均一致性指标
	        if(cr>=0.2){
	            System.out.println("WEIGHTS NOT RELIABLE");
	        }else{
	            //输出特征向量
	            for(int i=0;i<size;i++){
	                System.out.println("Criteria"+i +" weights："+w[i]);
	            }
	        }
	        //return weights;
	    }	        
	 }

