package test;
//package backtesting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

	public class MarketReturn {
		public ArrayList<Double> BenchmarkRatio = new ArrayList<Double>();
		public double marketReturnAverage;
		public Date startingDate;
		public Date closingDate;
		public ArrayList<Date> tradingDates = new ArrayList<Date>();
		
		public MarketReturn(Date start, Date close){
			startingDate = start;
			//System.out.println(startingDate + "***");
			closingDate = close;
		}
	public void computeRatio() {
	//read .csv
	String fileName = "/Users/ljt/Desktop/dissertation/backtesting_Java/backtesting/doc/S&P500.csv";
	File file = new File(fileName);//read about file
	//System.out.println(file);
		try{
			Scanner inputStream = new Scanner (file);
			inputStream.next();//ignore the first line
			inputStream.next();
//			for(int i=0;i<7;){
//				String data = inputStream.next();
//				String[] values = data.split(",");
//				System.out.println(values[0]);
//				i++;			
//			}
			
			while (inputStream.hasNext()){
				String data =  inputStream.next();//get a whole line
				String[] values=data.split(",");
				
				String dates = values[0];
				//System.out.println(dates + "***");
				DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				try{
					date = formatter.parse(dates);
                    //Date tradingdate = date;
				    //System.out.println(tradingdate + "*&*");
				}
				catch(ParseException e){
					e.printStackTrace();
				}
				if(date.compareTo(startingDate) >=0 && closingDate.compareTo(date) >=0){
					double startPrice = Double.parseDouble(values[1]);
					double closingPrice = Double.parseDouble(values[6]);
					this.tradingDates.add(date);
					this.BenchmarkRatio.add(closingPrice/startPrice - 1);
//				    for (int i=0; i<BenchmarkRatio.size();i++){
//				        System.out.println(BenchmarkRatio.get(i));
//				      }
				    
				}
				
                
				
				//Double closingPrice = Double.parseDouble(values[1]);
				//Double closingPrice = Double.parseDouble(values[6].replace("\"", ""));
				//System.out.println(dates);
				}
			double sum = 0;
			for (int i = 0; i < BenchmarkRatio.size();i++){
	            sum += BenchmarkRatio.get(i);
			}
			System.out.println("Benchmark Size" + BenchmarkRatio.size()+"$$$");
			
			marketReturnAverage = sum/BenchmarkRatio.size();
			//System.out.println(marketReturnAverage);
			//System.out.println(BenchmarkRatio.size());
			inputStream.close();
	    } 
		catch(FileNotFoundException e){
		}
	}
	public static void main(String[] args){
		DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String startDate = "2013/01/1";
		String closeDate = "2015/05/1";
		Date start = new Date();
		Date close = new Date();
		try{
			start = formatter.parse(startDate);
			close = formatter.parse(closeDate);		
			}
		catch(ParseException e){
			e.printStackTrace();
		}
		MarketReturn marketReturn = new MarketReturn(start, close);
		marketReturn.computeRatio();
	    double sum = 0;
		int numberOfPrices = 0;
		for (int i = 0; i < marketReturn.BenchmarkRatio.size();i++){
            sum += marketReturn.BenchmarkRatio.get(i);
            numberOfPrices++;			
//			System.out.println(marketReturn.BenchmarkRatio.get(i));
			
		}
		System.out.println("Average = " + (sum/numberOfPrices));
	}
	
}